Source: jruby
Section: ruby
Priority: optional
Maintainer: Raul Tambre <raul.tambre@clevon.com>
Homepage: https://www.jruby.org/
Vcs-Browser: https://gitlab.com/clevon/debian/jruby
Vcs-Git: https://gitlab.com/clevon/debian/jruby.git
Rules-Requires-Root: no
Standards-Version: 4.6.0
Build-Depends:
 debhelper-compat (= 13),
 default-jdk,
 javahelper,
 jruby-openssl,
 junit4 <!nocheck>,
 libasm-java,
 libawaitility-java,
 libbackport9-java,
 libbcpkix-java,
 libbcprov-java,
 libbctls-java,
 libbcutil-java,
 libbsf-java,
 libbuild-helper-maven-plugin-java,
 libdirgra-java,
 libexec-maven-plugin-java,
 libheadius-options-java,
 libinvokebinder-java,
 libjakarta-annotation-api-java,
 libjarjar-java <!nocheck>,
 libjcodings-java,
 libjffi-java,
 libjitescript-java,
 libjline2-java,
 libjnr-constants-java,
 libjnr-enxio-java,
 libjnr-ffi-java,
 libjnr-netdb-java,
 libjnr-posix-java,
 libjnr-unixsocket-java,
 libjoda-time-java,
 libjruby-joni-java,
 libjzlib-java,
 liblivetribe-jsr223-java <!nocheck>,
 libmaven-antrun-plugin-java,
 libmaven-assembly-plugin-java,
 libmaven-bundle-plugin-java,
 libmaven-copy-rename-plugin-java,
 libmaven-dependency-plugin-java,
 libmaven-exec-plugin-java,
 libmaven-invoker-plugin-java,
 libmaven-jar-plugin-java,
 libmaven-resources-plugin-java,
 libmaven-shade-plugin-java,
 libmaven-source-plugin-java,
 libosgi-core-java,
 libtakari-polyglot-maven-plugin-java,
 libtakari-polyglot-ruby-java,
 libyaml-snake-java,
 maven-debian-helper,
 rake,
 ruby-cmath,
 ruby-jar-dependencies,
 ruby-json,
 ruby-rake-ant,
 ruby-rspec,
 ruby-scanf,
 ruby-test-unit,

Package: jruby
Architecture: all
Depends:
 default-jre-headless,
 libbcpkix-java,
 libbcprov-java,
 libbctls-java,
 libbcutil-java,
 libjansi-java,
 libjffi-jni,
 libjline2-java,
 libruby3.1,
 libyaml-snake-java,
 ${maven:Depends},
 ${misc:Depends},
Recommends: jruby-openssl, ri, ruby-ffi, ruby-json, ruby-rspec, ruby-test-unit
Suggests: ${maven:OptionalDepends}
Description: 100% pure-Java implementation of Ruby
 JRuby is an implementation of the ruby language using the JVM.
 .
 It aims to be a complete, correct and fast implementation of Ruby, at the
 same time as providing powerful new features such as concurrency without a
 global interpreter lock, true parallelism and tight integration to the Java
 language to allow one to use Java classes in Ruby programs and to allow
 JRuby to be embedded into a Java application.
 .
 JRuby can be used as a faster version of Ruby, it can be used to run Ruby
 on the JVM and access powerful JVM libraries such as highly tuned concurrency
 primitives, it can also be used to embed Ruby as a scripting language in your
 Java program, or many other possibilities.
